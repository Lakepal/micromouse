
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.event.*;
import java.lang.*;
import java.math.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.Stack;

class AI extends Thread 
{
	int speed = 100;
	boolean sleep = false;
	Maze maze;

	AI(Maze m)
	{
		maze = m;
		init();
                
	}

	public void setSpeed(int spd)
	{
		speed = spd;
	}
	public void run()
	{
            int count = 0;
            
		while(true)
		{
			try
			{
				Thread.sleep(1000-speed);
			}catch(InterruptedException e)
			{
			}

			int exit = move();
			
			if(exit == 1) {
                                //System.out.print("\nexited!\nState: ");
                                //System.out.print(state);
                                count++;
                                maze.Rx = 0;
                                maze.Ry = 15;
                                state = 0;
                                steps = 0;
                                for(int y = 0; y < 16; y++)
                                {
                                    int start = 15 - y;
                                        for(int x = 0; x < 16; x++)
                                        {
                                            repeat[x][y] = 0;
                                        }
                                }
                                if(count == 5){
                                    break;
                                }  
			}
			
			maze.repaint();

			while(sleep)
			{
				try
				{
					Thread.sleep(10);
				}catch(InterruptedException e)
				{
				}
			}
		}
        }

	int position[][] = new int[16][16];
	int middleFloodAr[][] = new int[16][16];
	int startAr[][] = new int[16][16];
	int repeat[][] = new int[16][16];
        int updateFloodAr[][] = new int[16][16];
        Stack<Integer> x = new Stack();
        Stack<Integer> y = new Stack();


	private void init()
	{

		int max = 14;
		int min = 7;

		for(int y = 0; y < 16; y++)
		{
			int cnt = min;
			for(int x = 0; x < 16; x++)
			{
				if(x < 8)
				{
					middleFloodAr[x][y] = max-x;
				}else
				{
					middleFloodAr[x][y] = cnt;
					cnt++;
				}


			
			}
			if(y < 7)
			{
				max--;
				min--;
			}else if(y > 7)
			{
				max++;
				min++;
			}

		}

		for(int y = 0; y < 16; y++)
		{
			int start = 15 - y;
			for(int x = 0; x < 16; x++)
			{
				repeat[x][y] = 0;
				startAr[x][y] = start + x;
			}
		}
		position = middleFloodAr;
                /*
                System.out.println("startAr:");
                        System.out.println("");
                        for(int y = 0; y < 16; y++)
			{
				int start = 15 - y;
				for(int x = 0; x < 16; x++)
				{
					if (startAr[x][y] < 10)
                                        {
                                            System.out.print(startAr[x][y] + "  ");
                                        } else {
					System.out.print(startAr[x][y] + " ");
                                        }
				}
				System.out.println("");
			}
                */
	}

	int state = 0;
	int last = 0;
	int i = 0;
        int steps = 0;
        int currX, currY, dValue, minDist;
        
	private int move()
	{
                //System.out.print("\n\nMOVE START!\n");
		repeat[maze.Rx][maze.Ry]++;
                steps++;
                //System.out.print("State: ");
                //System.out.print(state);
                //System.out.println("");

		if(state == 0 && position[maze.Rx][maze.Ry] == 0)
		{
			System.out.println("Mouse reached to its destination");
			System.out.println("");
			System.out.println("Path of the Mouse and number of times mouse entered each box.");
			System.out.println("");
                        System.out.println("amount of steps taken: ");
                        System.out.println(steps);
                        System.out.println("");
			state = 1;
			position = middleFloodAr;
			System.out.print("\nRepeat:\n");
			for(int y = 0; y < 16; y++)
			{
				int start = 15 - y;
				for(int x = 0; x < 16; x++)
				{
					
					System.out.print(repeat[x][y] + " ");
				}
				System.out.println("");
			}
                        System.out.println("\nMiddleFloodAR:");
                        for(int y = 0; y < 16; y++)
			{
				int start = 15 - y;
				for(int x = 0; x < 16; x++)
				{
					if (middleFloodAr[x][y] < 10)
                                        {
                                            System.out.print(middleFloodAr[x][y] + "  ");
                                        } else {
					System.out.print(middleFloodAr[x][y] + " ");
                                        }
				}
				System.out.println("");
			}
                        
			return 1;
		}

		boolean right = false;
		boolean left = false;
		boolean up = false;
		boolean down = false;
                // check for walls
		if(!maze.getTop(maze.Rx, maze.Ry))
		{
                        //System.out.print("up is true\n");
			up = true;
		}
		if(!maze.getRight(maze.Rx, maze.Ry))
		{
                        //System.out.print("right is true\n");
			right = true;
		}
		if(!maze.getLeft(maze.Rx, maze.Ry))
		{
                        //System.out.print("left is true\n");
			left = true;
		}
		if(!maze.getBottom(maze.Rx, maze.Ry))
		{
                        //System.out.print("down is true\n");
			down = true;
		}
                /*
                System.out.println("");
                        System.out.println("MiddleFloodAR:");
                        System.out.println("");
                        for(int y = 0; y < 16; y++)
			{
				int start = 15 - y;
				for(int x = 0; x < 16; x++)
				{
					if (middleFloodAr[x][y] < 10)
                                        {
                                            System.out.print(middleFloodAr[x][y] + "  ");
                                        } else {
					System.out.print(middleFloodAr[x][y] + " ");
                                        }
				}
				System.out.println("");
			}
                */
                x.push(maze.Rx);
                y.push(maze.Ry);
                while(!x.empty() && !y.empty()) {
                    currX = x.pop();
                    currY = y.pop();
                    right = false;
                    left = false;
                    up = false;
                    down = false;
                    minDist = 1000;
                    /*
                    System.out.print("Current (x, y): (");
                    System.out.print(currX);
                    System.out.print(", ");
                    System.out.print(currY);
                    System.out.print(")\n");
                    */
                    dValue = middleFloodAr[currX][currY];
                    /*
                    System.out.print("Distance Value: ");
                    System.out.print(dValue);
                    System.out.println("");
                    System.out.println(!maze.getTop(currX, currY));
                    System.out.println(!maze.getLeft(currX, currY));
                    System.out.println(!maze.getBottom(currX, currY));
                    System.out.println(!maze.getRight(currX, currY));
                    */
                    if(!maze.getTop(currX, currY))
                    {
                        //System.out.println("UP is minimum");
                        minDist = middleFloodAr[currX][currY-1];
                        up = true;
                    }
                    if(!maze.getLeft(currX, currY))
                    {
                        if (middleFloodAr[currX-1][currY] < minDist)
                        {
                            //System.out.println("LEFT is minimum");
                            minDist = middleFloodAr[currX-1][currY];
                        }
                        left = true;
                    }
                    if(!maze.getBottom(currX, currY))
                    {
                        if (middleFloodAr[currX][currY+1] < minDist)
                        {
                            //System.out.println("DOWN is minimum");
                            minDist = middleFloodAr[currX][currY+1];
                        }
                        down = true;
                    }
                    if(!maze.getRight(currX, currY))
                    {
                        if (middleFloodAr[currX+1][currY] < minDist)
                        {
                            //System.out.println("RIGHT is minimum");
                            minDist = middleFloodAr[currX+1][currY];
                        }
                        right = true;
                    }
                    /*
                    System.out.print("Minimum Distance Value: ");
                    System.out.print(minDist);
                    System.out.println("");
                    
                    System.out.print("dValue - 1 = ");
                    System.out.print(dValue - 1);
                    System.out.println("");
                    */
                    if (dValue - 1 != minDist) {
                        //System.out.print("New cell value: ");
                        //System.out.print(minDist + 1);
                        //System.out.println("");
                        middleFloodAr[currX][currY] = minDist + 1;
                        
                        if(up && currY-1 != -1 && middleFloodAr[currX][currY-1] != 0){
                            //System.out.println("pushed up");
                            x.push(currX); y.push(currY-1); //up
                        }
                        if(left && currX-1 != -1 && middleFloodAr[currX-1][currY] != 0){
                            //System.out.println("pushed left");
                            x.push(currX-1); y.push(currY); //left
                        }
                        if(down && currY+1 != 16 && middleFloodAr[currX][currY+1] != 0 && currY+1 != 16){
                            //System.out.println("pushed down");
                            x.push(currX); y.push(currY+1); //down
                        }
                        if(right && currX+1 != 16 && middleFloodAr[currX+1][currY] != 0){
                            //System.out.println("pushed right");
                            x.push(currX+1); y.push(currY); //right
                        }
                    }
                }
                        
                /*
		if(state == 0 || state == 1) // state is 0 or 1 
		{


			int best = 10000;

			if(up)
			{
				if(repeat[maze.Rx][maze.Ry-1] < best)
				{
					best = repeat[maze.Rx][maze.Ry-1];
                                        steps++;
				}else if(repeat[maze.Rx][maze.Ry-1] > best)
				{
					up = false;
				}
			}
			if(right)
			{
				if(repeat[maze.Rx+1][maze.Ry] < best)
				{
					best = repeat[maze.Rx+1][maze.Ry];
                                        steps++;
					up = false;
				}else if(repeat[maze.Rx+1][maze.Ry] > best)
				{
					right = false;
				}
			}
			if(left)
			{
				if(repeat[maze.Rx-1][maze.Ry] < best)
				{
					up = false;
					right = false;
					best = repeat[maze.Rx-1][maze.Ry];
                                        steps++;
				}else if(repeat[maze.Rx-1][maze.Ry] > best)
				{
					left = false;
				}
			}
			if(down)
			{
				if(repeat[maze.Rx][maze.Ry+1] < best)
				{
					up = false;
					right = false;
					left = false;
					best = repeat[maze.Rx][maze.Ry+1];
                                        steps++;
				}else if(repeat[maze.Rx][maze.Ry+1] > best)
				{
					down = false;
				}
			}
                        */
			int best = 500;
			if(up)
			{
				if(maze.Ry-1 != -1 && middleFloodAr[maze.Rx][maze.Ry-1] < best)
				{
					best = middleFloodAr[maze.Rx][maze.Ry-1];
                                        //steps++;
				}else if(maze.Ry-1 != -1 && middleFloodAr[maze.Rx][maze.Ry-1] > best)
				{
					up = false;
				}
			}
			if(right)
			{
				if(maze.Rx+1 != 16 && middleFloodAr[maze.Rx+1][maze.Ry] < best)
				{
					up = false;
					best = middleFloodAr[maze.Rx+1][maze.Ry];
                                        //steps++;
				}else if(maze.Rx+1 != 16 && middleFloodAr[maze.Rx+1][maze.Ry] > best)
				{
					right = false;
				}
			}
			if(left)
			{
				if(maze.Rx-1 != -1 && middleFloodAr[maze.Rx-1][maze.Ry] < best)
				{
					up = false;
					right = false;
					best = middleFloodAr[maze.Rx-1][maze.Ry];
								if(i == 167)
								{
									System.out.println(best);
			}
				}else if(maze.Rx-1 != -1 && middleFloodAr[maze.Rx-1][maze.Ry] > best)
				{
					left = false;
				}
			}
			if(down)
			{
				if(maze.Ry+1 != 16 && middleFloodAr[maze.Rx][maze.Ry+1] < best)
				{
					up = false;
					right = false;
					left = false;
					best = middleFloodAr[maze.Rx][maze.Ry+1];
                                        //steps++;
				}else if(maze.Ry+1 != 16 && middleFloodAr[maze.Rx][maze.Ry+1] > best)
				{
					down = false;
				}
			}


		//}
                
                


		if(i == 167)
		{
			System.out.println(i + ": " + left);
		}


		if(up && last == 1)
		{
			right = false;
			left = false;
			down = false;
		}else if(right && last == 2)
		{
			up = false;
			left = false;
			down = false;
		}else if(left && last == 3)
		{
			up = false;
			right = false;
			down = false;
		}else if(down && last == 4)
		{
			up = false;
			right = false;
			left = false;
		}


		i++;
		if(up)
		{
			last = 1;
			maze.moveUp();
			Direction.getHeadDirection(true, false, false, false);
		}else if(right)
		{
			last = 2;
			maze.moveRight();
			Direction.getHeadDirection(false, true, false, false);
		}else if(left)
		{
			last = 3;
			maze.moveLeft();
			Direction.getHeadDirection(false, false, false, true);
		}else
		{
			last = 4;
			maze.moveDown();
			Direction.getHeadDirection(false, false, true, false);
		}
		return 0;
	}
}

class MicroMouse extends JFrame implements ActionListener, ChangeListener
{
	JFrame main;
	JPanel btn;
	Maze maze;
	AI ai;

	int speed = 100;

	public MicroMouse()
	{
		main = new JFrame("MicroMouse");
		btn = new JPanel();
		main.setSize(450, 550);
		main.setLayout(new BorderLayout());
		main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Maze maze = new Maze();
		main.getContentPane().add(maze, BorderLayout.LINE_START);

		JSlider spd = new JSlider(JSlider.HORIZONTAL, 0, 1000, 650);
		spd.addChangeListener(this);
		main.getContentPane().add(spd, BorderLayout.PAGE_END);

		main.getContentPane().add(btn, BorderLayout.LINE_END);

                main.setVisible(true);

                maze.LoadMaze();
		//maze.moveUp();
                ai = new AI(maze);
                maze.Rx = 0;
		maze.Ry = 15;
                ai.state = 0;
                maze.moveUp();
                //for(int i = 0; i < 5; i++){
                ai.run();
                //  }


	}


	public static void main(String arg[])
	{
		new MicroMouse();
	}

	public void actionPerformed(ActionEvent e)
	{
	}

	public void stateChanged(ChangeEvent e)
	{
	    JSlider source = (JSlider)e.getSource();
	    if (!source.getValueIsAdjusting())
	    {
	    	ai.setSpeed((int)source.getValue());

	    }

	}
}


class Maze extends JPanel
{
	boolean top[][] = new boolean[16][17];
	boolean side[][] = new boolean [17][16];
        
	int Rx = 0;
	int Ry = 15;
	int Sx = 0;
	int Sy = 15;

	int x = 300;
	
	Map<Integer,Integer> mouseMap = new HashMap<Integer, Integer>();
	
	public boolean redrawPath = false;

	protected void paintComponent(Graphics g)
	{
		setSize(430,430);
		g.setColor(Color.WHITE);
	    g.fillRect(0, 0, getWidth(), getHeight());
	    g.setColor(Color.BLACK);
	    for(int y = 0; y < 17; y++)
	    {
			for(int x = 0; x < 16; x++)
			{
				if(top[x][y])
				{
					g.drawLine(x*26+5,y*26+5, x*26+31,y*26+5);
				}
			}
		}

		for(int y = 0; y < 16; y++)
		{
			for(int x = 0; x < 17; x++)
			{
				if(side[x][y])
				{
					g.drawLine(x*26+5,y*26+5, x*26+5,y*26+31);
				}
			}
		}

		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(6+(Sx*26), 6+(Sy*26), 25, 25);

		g.setColor(Color.CYAN);
		g.fillRect(6+(7*26), 6+(7*26), 51, 51);

		g.setColor(Color.ORANGE);
		g.fillRect(9+(Rx*26), 9+(Ry*26), 19, 19);
		
		if(Direction.mouseHead.equals("N")) {
			g.setColor(Color.BLACK);
			g.fillRect(9+(Rx*26), 9+(Ry*26), 19, 5);
		}
		if(Direction.mouseHead.equals("E")) {
			g.setColor(Color.BLACK);
			g.fillRect(24+(Rx*26), 9+(Ry*26), 5, 19);
		}
		if(Direction.mouseHead.equals("W")) {
			g.setColor(Color.BLACK);
			g.fillRect(5+(Rx*26), (9+Ry*26), 5, 19);
		}
		if(Direction.mouseHead.equals("S")) {
			g.setColor(Color.BLACK);
			g.fillRect((9+Rx*26), (24+Ry*26), 19,5);
		}
	}

	void setStart(int x, int y)
	{
		Sx = x;
		Sy = y;
	}
	boolean[][] getTop()
	{
		return top;
	}

	boolean[][] getSide()
	{
		return side;
	}

	void printMaze()
	{
		for(int y = 0; y < 16; y++)
		{
			System.out.print(" ");
			for(int x = 0; x < 16; x++)
			{
				if(top[x][y])
				{
					System.out.print("- ");
				}else
				{
					System.out.print("  ");
				}
			}
			System.out.println("");
			for(int x = 0; x < 17; x++)
			{
				if(x == Rx && y == Ry)
				{
					if(side[x][y])
					{
						System.out.print("|0");
					}else
					{
						System.out.print(" 0");
					}
				}else
				{
					if(side[x][y])
					{
						System.out.print("| ");
					}else
					{
						System.out.print("  ");
					}
				}
			}
			System.out.println("");
		}

		System.out.print(" ");
		for(int x = 0; x < 16; x++)
		{
			if(top[x][16])
			{
				System.out.print("- ");
			}else
			{
				System.out.print("  ");
			}
		}
		System.out.println("");
	}

	void LoadMaze()
	{

		File fin = new File("../InputTextFiles/maze2.txt");
		try
		{
			FileInputStream in = new FileInputStream (fin);
			char let;
			let = (char)in.read();
			boolean flag = true;
			boolean spaces = false;
			int x = 0;
			int y = 0;
			while((byte)let != -1)
			{
				if(flag && x < 16)
				{
					if(spaces)
					{
						if(let == '-' || let == '_')
						{
							top[x][y] = true;
							x++;
						}else
						{
							top[x][y] = false;
							x++;
						}
						spaces = false;
					}else
					{
						spaces = true;
					}
				}else if(!flag && x < 17)
				{
					if(!spaces)
					{
						if(let == '|')
						{
							side[x][y] = true;
							x++;
						}else
						{
							side[x][y] = false;
							x++;
						}
						spaces = true;
					}else
					{

					spaces = false;
					}
				}
				if(let == '\n')
				{
					flag = !flag;
					if(flag == true)
					{
						y++;
					}
					x = 0;
					spaces = false;
				}
				let = (char)in.read();
			}
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		for(int y = 0; y < 16; y++)
		{
			for(int x = 0; x < 17; x++)
			{
				if(side[x][y])
				{
				}else
				{
				}
			}
		}

	}

	boolean getTop(int x, int y)
	{
		return top[x][y];
	}

	boolean getBottom(int x, int y)
	{
		return top[x][y+1];
	}

	boolean getLeft(int x, int y)
	{
		return side[x][y];
	}

	boolean getRight(int x, int y)
	{
		return side[x+1][y];
	}

	void setRx(int x)
	{
		Rx = x;
	}

	void setRy(int y)
	{
		Ry = y;
	}

	void setPos(int x, int y)
	{
		Ry = y;
		Rx = x;
	}

	int getRx()
	{
		return Rx;
	}

	int getRy()
	{
		return Ry;
	}

	boolean moveUp()
	{
		if(!getTop(Rx, Ry))
		{
			Ry--;
			return true;
		}else
		{
			return false;
		}
	}

	boolean moveDown()
	{
		if(!getBottom(Rx, Ry))
		{
			Ry++;
			return true;
		}else
		{
			return false;
		}
	}

	boolean moveRight()
	{
		if(!getRight(Rx, Ry))
		{
			Rx++;
			return true;
		}else
		{
			return false;
		}
	}

	boolean moveLeft()
	{
		if(!getLeft(Rx, Ry))
		{
			Rx--;
			return true;
		}else
		{
			return false;
		}
	}        
}


